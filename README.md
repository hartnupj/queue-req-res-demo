# Queue demo

Simple demo of a bunch of very micro services collaborating via queues to
service an HTTP request.

Pre-reqs: Node.js, yarn, Docker, Docker Compose.

For the purposes of the demo, just run each service in a different terminal
window:

-   BusMQ is the queue implementation. It uses Redis. `docker-compose up`
    will bring up a suitable Redis instance.
-   `yarn install` to install dependencies
-   `cd src; node express-queue-demo.js` will start express
-   Visit <http://localhost:3080/greeting/adam> -- see how Express times out the
    request because nothing is processing the queue
-   In more terminals:
    -   `node filters/name-capitaliser.js`
    -   `node routers/importance-router.js`
    -   `node filters/greeting-decorator.js`
    -   `node filters/fancy-greeting-decorator.js`
    -   `node filters/responder.js`

Now when you visit <http://localhost:3080/greeting/adam> you should see a
proper response.

The `importance-router` chooses a destination queue based on `name`. It has a
very simple criterion. Try changing the `adam` to `john` in the URL.

# Request-reply

The demo as a whole illustrates the **Request-Reply Pattern** - the requestor
creates its own response queue, with a dynamically generated name, which it will
reuse for all its requests.

Each request message contains **both** fields:

-   `replyTo`: the name of the response queue
-   `correlationId`: a new one is created for each request

The requestor reads responses from the response queue, and forwards them as
responses to the right HTTP request by looking up the correlationId.

`correlationId` -> `res` mappings are held in a cache, to be removed either when the response arrives, or after a timeout.

The responder simply gets the `replyTo` from the body, and
forwards to that queue. The responder isn't quite optimal in
this demo, because it re-attaches to the response queue each
time. It would be an improvement if it cached and reused the
connection.

# Notes

-   BusMQ was completely arbitrarily chosen; I don't know whether or not it's
    suitable for any particular purpose
-   The microservices are hardcoded with the queues from/to which they
    read/write -- this isn't how things should be. At the very least it should
    be in configuration. Better, "infrastructure" would route the messages.
-   `content-based-router.js` and `translator.js` are general
     implementations of these two patterns, so the actual microservices have
     minimal code other than their actual "business" logic.
- You can run multiple instances of any of the microservices to scale horizontally.
