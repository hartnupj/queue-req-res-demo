const Bus = require('busmq');

var bus = Bus.create({redis: ['redis://127.0.0.1:6379']});

bus.on('error', err => {
  console.error(`Bus error: ${err}`);
});

bus.on('offline', () => {
  console.log('offline');
  process.exit(0);
});

bus.on('online', () => {
  const requester = bus.service('foo');

  requester.connect(() => {
    console.log('connected to the service');
  });

  requester.request({hello: 'world'}, (err, reply) => {
    console.log('the service replied with ' + reply.thisis);
    console.log('disconnecting');
    bus.disconnect();
  });
});

bus.on('online', function() {
  var handler = bus.service('foo');
  handler.on('request', (request, reply) => {
    console.log('Hey! a new request just got in: ' + request.hello);
    reply(null, {thisis: 'my reply'});
  });
  // start serving requests
  handler.serve(() => {
    console.log('serving. requests will soon start flowing in...');
  });
});

bus.connect();
