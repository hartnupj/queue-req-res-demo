const monitor = require('./queue-monitor');

module.exports = (bus, inQueueName, outQueueName, fn) => {
  bus.on('online', () => {
    const inQueue = bus.queue(inQueueName);
    const outQueue = bus.queue(outQueueName);

    monitor(inQueue);
    monitor(outQueue);

    inQueue.on('message', (payload, id) => {
      console.log(`recv (${id}): ${payload}`);
      const msg = JSON.parse(payload);
      const filtered = fn(msg);
      console.log(`send: ${JSON.stringify(filtered)}`);
      outQueue.push(filtered);
    });

    outQueue.on('attached', () => {
      inQueue.attach();
      inQueue.consume();
    });

    outQueue.attach();
  });
};
