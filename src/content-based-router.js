const monitor = require('./queue-monitor');
const R = require('ramda');


module.exports = (bus, inQueueName, predicatesAndOutQueueNames) => {
  const toPushConditions = ([predicate, name]) => {
    const queue = bus.queue(name);
    monitor(queue);
    queue.attach();
    return [predicate, msg => {
      console.log(`Routing to ${queue.name}`);
      queue.push(msg);
    }];
  };

  bus.on('online', () => {
    const inQueue = bus.queue(inQueueName);
    monitor(inQueue);

    const conditions = R.map(toPushConditions)(predicatesAndOutQueueNames);

    inQueue.on('message', (payload, id) => {
      console.log(`recv (${id}): ${payload}`);
      const msg = JSON.parse(payload);
      R.cond(conditions)(msg);
    });

    inQueue.attach();
    inQueue.consume();
  });
};
