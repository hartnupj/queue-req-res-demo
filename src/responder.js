const Bus = require('busmq');
const monitor = require('./queue-monitor');

const bus = Bus.create({ redis: ['redis://127.0.0.1:6379'] });

bus.on('online', () => {
  const inQueue = bus.queue('complete');
  monitor(inQueue);

  inQueue.on('message', (payload, id) => {
    const msg = JSON.parse(payload);
    console.log(`recv (${id}): ${payload}`);
    const qname = msg.replyTo;
    const responseQ = bus.queue(qname);
    monitor(responseQ);
    responseQ.on('attached', () => {
      console.log(`send: ${JSON.stringify(msg)}`);
      responseQ.push(msg);
      responseQ.detach();
    });

    responseQ.attach();
  });

  inQueue.attach();
  inQueue.consume();
});

bus.connect();
