const Bus = require('busmq');

const bus = Bus.create({redis: ['redis://127.0.0.1:6379']});

// Service:
bus.on('online', function() {
  const handler = bus.queue('service');

  handler.on('attached', (exists) => {
    console.log(`Handler attached: ${exists ? 'exists' : 'new'}`);
  });

  handler.on('message', (payload, id) => {
    const msg = JSON.parse(payload);
    console.log(`received request ${id}: ${payload}`);
    const qname = msg.replyTo;
    console.log(`Response queue name: ${qname}`);
    const responseQ = bus.queue(qname);
    responseQ.on('attached', (exists) => {
      console.log(`Attached to response queue: ${exists ? 'exists' : 'new'}`);
      responseQ.push(`Hello ${msg.name}`);
      responseQ.detach();
    });

    responseQ.on('detached', () => {
      console.log('Detached from response queue');
    });

    responseQ.attach();

  });

  handler.attach();
  handler.consume();
});

bus.connect();
