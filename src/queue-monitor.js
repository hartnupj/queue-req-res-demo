module.exports = queue => {
  queue.on('detached', () => {
    console.log(`Detached from queue ${queue.name}`);
  });

  queue.on('attached', (exists) => {
    console.log(`Attached to queue ${queue.name} - ${exists ? 'exists' : 'new'}`);
  });

};
