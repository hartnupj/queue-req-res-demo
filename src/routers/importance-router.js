const contentBasedRouter = require('../content-based-router');
const Bus = require('busmq');
const R = require('ramda');

const bus = Bus.create({ redis: ['redis://127.0.0.1:6379'] });

const isJohn = R.compose(
  R.equals('John'),
  R.prop('name')
);

contentBasedRouter(bus, 'capitalised', [
  [isJohn, 'important'],
  [R.T, 'unimportant'],
]);

bus.connect();
