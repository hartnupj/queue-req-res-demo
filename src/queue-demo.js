const Bus = require('busmq');
const uuidV3 = require('uuid/v4');

const bus = Bus.create({ redis: ['redis://127.0.0.1:6379'] });

bus.on('error', err => {
  console.error(`Bus error: ${err}`);
});

bus.on('offline', () => {
  console.log('offline');
  process.exit(0);
});

bus.on('online', () => {
  const uuid = `${uuidV3()}`;
  console.log(uuid);
  const responseQ = bus.queue(uuid);

  responseQ.on('attached', exists => {
    console.log(`Attached to response queue ${exists ? 'exists' : 'new'}`);
  });

  responseQ.on('message', msg => {
    console.log(`Response received: ${msg}`);
    responseQ.detach();
    process.exit(0);
  });

  responseQ.on('detached', () => {
    console.log('Detached from response queue');
  });

  responseQ.attach();
  responseQ.consume();

  const requestQ = bus.queue('service');

  requestQ.attach();
  requestQ.push({ replyTo: uuid, name: 'John' });
});

bus.connect();
