const Bus = require('busmq');
const translator = require('../translator');
const R = require('ramda');

const bus = Bus.create({ redis: ['redis://127.0.0.1:6379'] });

translator(bus, 'important', 'complete', R.converge(
  R.assoc('body'),
  [
    R.compose(R.concat('A thousand blessings '), R.prop('name')),
    R.identity
  ]
));

bus.connect();
