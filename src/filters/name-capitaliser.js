const Bus = require('busmq');
const translator = require('../translator');
const { lensProp, over } = require('ramda');
const namecase = require('namecase');

const bus = Bus.create({redis: ['redis://127.0.0.1:6379']});

const lens = lensProp('name');

translator(bus, 'service', 'capitalised', over(lens, namecase));

bus.connect();
