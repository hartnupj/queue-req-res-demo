const Bus = require('busmq');
const uuidV4 = require('uuid/v4');
const monitor = require('./queue-monitor');
const NodeCache = require('node-cache');
const mustache = require('mustache');
const express = require('express');

const bus = Bus.create({ redis: ['redis://127.0.0.1:6379'] });
const app = express();

const pendingResponses = new NodeCache({
  stdTTL: 5,
  checkperiod: 1,
  useClones: false,
});

pendingResponses.on('expired', (key, res) => {
  console.log(`timeout on ${key}`);
  res.status(504);
  res.send('<h1>504 Timeout</h1><p>The request timed out</p>');
  res.end();
});

const sendToCorrelatedExpressResponse = msg => {
  pendingResponses.get(msg.correlationId, (err, res) => {
    if (err) {
      console.error(err);
    } else if (res) {
      pendingResponses.del(msg.correlationId);
      res.send(mustache.render('<h1>This came from a queue</h1><p>{{body}}</p>\n', msg));
      res.end();
    } else {
      console.error('Received response for expired request');
    }
  });
};

bus.on('error', err => {
  console.error(`Bus error: ${err}`);
});

bus.on('online', () => {
  const responseQueueName = `response-${uuidV4()}`;
  const requestQ = bus.queue('service');
  const responseQ = bus.queue(responseQueueName);
  monitor(requestQ);
  monitor(responseQ);

  responseQ.on('message', (payload, id) => {
    console.log(`recv (${id}): ${payload}`);
    const msg = JSON.parse(payload);
    sendToCorrelatedExpressResponse(msg);
  });

  requestQ.on('attached', () => {
    app.listen(3080, () => console.log('Express is listening...'));
  });

  app.get('/greeting/:name', (req, res) => {
    const uuid = uuidV4();
    pendingResponses.set(uuid, res);
    const msg = { correlationId: uuid, replyTo: responseQueueName, ...req.params };
    console.log(`send: ${JSON.stringify(msg)}`);
    requestQ.push(msg);
  });

  requestQ.attach();
  responseQ.attach();
  responseQ.consume();
});

bus.connect();

